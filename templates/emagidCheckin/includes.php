<link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."main.css")?>">

<link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."material.css")?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel = "stylesheet" href = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel = "shortcut icon" href = "<?=auto_version(FRONT_ASSETS."img/american-favicon.png")?>" />

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?=auto_version(FRONT_JS."main.js")?>"></script>

<script src="<?=auto_version(FRONT_JS."material.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."nouislider.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."js.cookie.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."isotope.min.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."velocity.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."wNumb.js")?>"></script>
<!-- <script src="<?=auto_version(FRONT_JS."timeit.js")?>"></script> -->
<script src="<?=auto_version(FRONT_LIBS."/iscroll/iscroll.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."jquery.gdocsviewer.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."jquery.simplesidebar.js")?>"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">



<script src="<?=auto_version(FRONT_LIBS."jquery.zoom.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."gifshot/gifshot.js")?>"></script>
<script type="text/javascript" src="<?=auto_version(FRONT_LIBS."slick/slick/slick.min.js")?>"></script>
<link rel="stylesheet" type="text/css" href="<?=auto_version(FRONT_LIBS."slick/slick/slick.css")?>"/>
<link rel="stylesheet" type="text/css" href="<?=auto_version(FRONT_LIBS."slick/slick/slick-theme.css")?>"/>

<link rel="stylesheet" href="<?=auto_version(FRONT_LIBS."scrollbar/jquery.mCustomScrollbar.css")?>" />
<script src="<?=auto_version(FRONT_LIBS."scrollbar/jquery.mCustomScrollbar.concat.min.js")?>"></script>

<script src="<?=auto_version(FRONT_LIBS."slidesjs/jquery.slides.min.js")?>"></script>
<script src="https://momentjs.com/downloads/moment.min.js" ></script>

<? if ($this->emagid->route['controller'] == 'category') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."categories.css")?>">
    <script src="<?=auto_version(FRONT_JS."category.js")?>"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'checkout') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."checkout.css")?>">
    <script src="<?=auto_version(FRONT_JS."checkout.js")?>"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'cart') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."checkout.css")?>">
    <script src="<?=auto_version(FRONT_JS."checkout.js")?>"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'account') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."account.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'wishlist') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."wishlist.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'expertise') { ?>

    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."expertise.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'wholesale') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."wholesale.css")?>">
    <script src="<?=auto_version(FRONT_LIBS."jquery.maskedinput.js")?>" type="text/javascript"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'contact') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."contact.css")?>">
<? } ?>

<? if ($this->emagid->route['controller']== 'inquiry') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."inquiry.css")?>">
    <script type="text/javascript" src="/content/admin/js/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'about') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."about.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'emagidCheckin') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."wholesale.css")?>">
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."productinfo.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'pages') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."shippingReturns.css")?>">
     <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."expertise.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'cart') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."checkout.css")?>">
    <script src="<?=auto_version(FRONT_JS."checkout.js")?>"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'browse') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."browse_rings.css")?>">
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."search_create.css")?>">
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."categories.css")?>">
    <script src="<?=auto_version(FRONT_JS."category.js")?>"></script>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."product.css")?>">
    <script src="<?=auto_version(FRONT_JS."product.js")?>"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'diamonds') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."search_create.css")?>">
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."browse_rings.css")?>">
    <script src="<?=auto_version(FRONT_JS."search_create.js")?>"></script>
<? } ?>


<? if ($this->emagid->route['controller'] == 'products' || $this->emagid->route['controller'] == 'weddingbands' || $this->emagid->route['controller'] == 'rings' || $this->emagid->route['controller'] == 'jewelry') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."categories.css")?>">
    <script src="<?=auto_version(FRONT_JS."category.js")?>"></script>
    <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."product.css")?>">
    <script src="<?=auto_version(FRONT_JS."product.js")?>"></script>
<? } ?>

