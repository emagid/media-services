-- May 7, 2018
CREATE SEQUENCE public.email_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.email
(
  id integer NOT NULL DEFAULT nextval('email_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  email character varying,
  contact_id integer NOT NULL,
  CONSTRAINT email_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE SEQUENCE public.highlight_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.highlight
(
  id integer NOT NULL DEFAULT nextval('highlight_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  image character varying,
  display_order character varying,
  CONSTRAINT highlight_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- May 8, 2018

CREATE SEQUENCE public.hccsupport_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.hccsupport
(
  id integer NOT NULL DEFAULT nextval('hccsupport_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  support_list character varying,
  phone character varying,
  CONSTRAINT hccsupport_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE SEQUENCE public.map_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.map
(
  id integer NOT NULL DEFAULT nextval('map_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  map_image character varying,
  display_order character varying,
  CONSTRAINT map_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- May 9, 2018
CREATE SEQUENCE public.phone_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.phone
(
  id integer NOT NULL DEFAULT nextval('phone_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  phone character varying,
  contact_id integer NOT NULL,
  CONSTRAINT phone_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE SEQUENCE public.session_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.session
(
  id integer NOT NULL DEFAULT nextval('session_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  title character varying,
  date_time character varying,
  CONSTRAINT session_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

--May 10, 2018
CREATE SEQUENCE public.support_user_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.support_user
(
  id integer NOT NULL DEFAULT nextval('support_user_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  user_phone character varying,
  support_list_id integer,
  CONSTRAINT support_user_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE SEQUENCE public.about_us_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.about_us
(
  id integer NOT NULL DEFAULT nextval('about_us_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  description character varying,
  CONSTRAINT about_us_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE session ALTER COLUMN date_time TYPE INTEGER USING date_time::INTEGER;

-- SEP 9/13

CREATE TABLE public.video
(
  id serial,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  title character varying,
  image character varying,
  background_color character varying DEFAULT 'ffffffff',
  video character varying,
  page INTEGER,
  display_order INTEGER,
  CONSTRAINT video_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- Sep 18, 2018

ALTER TABLE page ALTER COLUMN content TYPE JSON USING content::JSON;
ALTER TABLE page ADD COLUMN template INTEGER;
ALTER TABLE question ADD COLUMN survey_id INTEGER;

CREATE TABLE public.kiosk
(
  id serial,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  name character varying,
  description character varying,
  slug character varying,
  home_page INTEGER,
  CONSTRAINT kiosk_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE menu
(
  id serial,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  title character varying,
  image character varying,
  type INTEGER,
  display_order INTEGER,
  kiosk_id INTEGER,
  CONSTRAINT menu_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE survey
(
  id serial,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  title CHARACTER VARYING,
  questions INTEGER[],
  display_order INTEGER,
  background_color character varying DEFAULT 'ffffffff',
  CONSTRAINT survey_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE response
(
  id serial,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  question_id INTEGER,
  answer_id INTEGER,
  kiosk_id INTEGER,
  survey_id INTEGER,
  session_id CHARACTER VARYING,
  CONSTRAINT response_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE question
(
  id serial,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  text CHARACTER VARYING,
  display_order INTEGER,
  CONSTRAINT question_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE answer
(
  id serial,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  text CHARACTER VARYING,
  question_id INTEGER,
  display_order INTEGER,
  CONSTRAINT answer_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- Sep 25, 2018

ALTER TABLE kiosk ADD COLUMN content JSON;
