<div class='content'>
  
  <section class='main home_page'>
    <div class='page_title'>
      <p class='lrg'>VIDEO WALL<span>SOLUTIONS</span></p>
    </div>

    <div class='projs'>
      <div data-page='wheel' class='icon click card wof'>
      <p class='lrg'>SPIN TO WIN</p>
        <img src="<?= FRONT_ASSETS ?>img/wheel_icon.png">
      </div>
      <div data-page='photobooth' class='icon click card selfie'>
        <p class='lrg'>TAKE A SELFIE</p>
        <img src="<?= FRONT_ASSETS ?>img/selfie.png">
      </div>
      <div data-page='projects' class='click card projects'>
        <div class='slider'>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj1.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj2.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj3.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj4.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj5.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj7.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj8.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj9.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj10.jpg')"></div>
          <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj11.jpg')"></div>
        </div>
        <p class='lrg'>VIEW ALL PROJECTS <span class='arr'>></span></p>
      </div>
    </div>
  </section>

  <script type="text/javascript">
    $(document).ready(function(){
      let slider = function slide(slide) {
            slide.fadeIn(1000);
            var nxt = checkSlide(slide);
            var timer = setTimeout(function(){
                slider(nxt);
                slide.fadeOut(1000);
            }, 3000);
        }

        function checkSlide(slide) {
            var nxt = slide.next('div');
            if ( nxt.length == 0 ) {
                return $('.slider > div:first-child')
            }else {
                return nxt;
            }
        }

        slider($('.slider > div:first-child'));
    });
  </script>

</div>