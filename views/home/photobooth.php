<div class='content'>
  <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."photobooth.css")?>">

  <section class='main photobooth_page'>
    <img class='home' src="<?= FRONT_ASSETS ?>img/home.png">
    <!-- Step 1 -->
    <div class='step_one'>
      <div class='background' style="background-image: url('<?=FRONT_ASSETS?>img/back.jpg')"></div>
      <img id='filter' src="<?=FRONT_ASSETS?>img/filter.jpg">
    		<video id="video"  autoplay></video>
            <device type="media" onchange="update(this.data)"></device>
            <script>
                function update(stream) {
                    document.querySelector('video').src = stream.url;
                }

                var video = document.getElementById('video');

                if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                        try {
                          video.src = window.URL.createObjectURL(stream);
                        } catch(error) {
                          video.srcObject = stream;
                        }
                        video.play();
                    });
                }

                $(document).on('click', function(){
                  var video = document.getElementById('video');

                if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                        try {
                          video.src = window.URL.createObjectURL(stream);
                        } catch(error) {
                          video.srcObject = stream;
                        }
                        video.play();
                    });
                }
                })
            </script>

      <div id='take_pic' class='button'>
        <p class='lrg'>TAKE A SELFIE</p>
      </div>
        <div class='countdown'>3</div>
        <div class='countdown'>2</div>
        <div class='countdown'>1</div>
    </div>

    <div class='flash'></div>


    <!-- Step 2 -->
    <div id='pictures' class='step_two'>
      <div class='btns'>
        <div class='retake button white_button'>
          <p class='lrg'>RETAKE</p>
        </div>
        <div class='share button'>
          <p class='lrg'>SHARE</p>
        </div>
      </div>
      <!-- <div class='card full'>
        <p class='sml'>Add some props to your picture!</p>
        <div class='props'>
          <img id='hat1' src="<?=FRONT_ASSETS?>img/hat.png">
          <img id='hat2' src="<?=FRONT_ASSETS?>img/glasses.png">
          <img id='hat3' src="<?=FRONT_ASSETS?>img/hat.png">
          <img id='hat4' src="<?=FRONT_ASSETS?>img/hat.png">
          <img id='hat5' src="<?=FRONT_ASSETS?>img/hat.png">
          <img id='hat6' src="<?=FRONT_ASSETS?>img/hat.png">
          <img id='hat7' src="<?=FRONT_ASSETS?>img/hat.png">
          <img id='hat8' src="<?=FRONT_ASSETS?>img/hat.png">
        </div>
      </div> -->
    </div>


    <!-- Step 3 -->
    <div class='step_three'>
      <div class='card full'>
        <i class="fa fa-close sharex" style="font-size:36px"></i>
        <form id='submit_form'>
          <input type='hidden' name='form' value="1">
            <input type="hidden" name="image" class="image_encoded">
            <span>
                <input class='input jQKeyboard first_email' name='email[]' pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" type='text' placeholder='Email' title='Please enter a valid email address.'>
            </span>
            <p id='add_email'>Add an email +</p>

            <span>
                <input class='input jQKeyboard first_phone' name="phone[]" placeholder="Phone Number (eg: +19874563210)">
            </span>
            <p id='add_phone'>Add a phone number +</p>
        </form>
      </div>

      <div id='share_btn' class='button'>
        <p class='lrg'>SEND PICTURE</p>
      </div>
    </div>

  </section>
        <!-- Alerts -->
  <section id='share_alert'>
    <h3>Thank you!</h3>
    <p>Your picture is on it's way</p>
  </section>

  <link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>jQKeyboard.css">
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.4.6/fabric.min.js'></script>
  <script src="<?=auto_version(FRONT_JS."photobooth.js")?>"></script>
  <script src="<?=auto_version(FRONT_JS."keyboard.js")?>"></script>
    <style type="text/css">
    div.jQKeyboardContainer {
      max-width: 977px;
      top: 0px !important;
    }
  </style>
  <script type="text/javascript">
    var keyboard;
            $(function(){
                keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                                ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                            ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});
                $('textarea.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.number_key').initKeypad({'donateKeyboardLayout': board});
            });
  </script>

</div>


  


