<div class='content'>

  <img class='home' src="<?= FRONT_ASSETS ?>img/home.png">
  <section class='main medical_page'>
    <div class='page_title'>
      <p class='sml'>How Popshap can help you</p>
      <p class='lrg'>MEDICAL SOLUTIONS</p>
    </div>

    <div class='card full' style='background-color: #ffc722'>
      <div style="background-image: url('<?= FRONT_ASSETS ?>img/wayfind.jpg');"></div>
      <!-- <p class='sml'>Helping patients navigate with ease</p> -->
      <p class='lrg'>WAYFINDING</p>
    </div>
    <div class='card full' style='background-color: #2eb4d1'>
      <div style="background-image: url('<?= FRONT_ASSETS ?>img/checkin.jpg');"></div>
      <!-- <p class='sml'>Speeing up the process of checking in</p> -->
      <p class='lrg'>CHECK-IN SYSTEMS</p>
    </div>
    <div class='card full' style='background-color: #ff5722'>
      <div style="background-image: url('<?= FRONT_ASSETS ?>img/info.jpg');"></div>
      <!-- <p class='sml'>Attention grabbing digital information</p> -->
      <p class='lrg'>INFO DISPLAY</p>
    </div>
    <div class='card full' style='background-color: #8bc34a'>
      <div style="background-image: url('<?= FRONT_ASSETS ?>img/directory.jpg');"></div>
      <!-- <p class='sml'>Finding rooms with a few clicks</p> -->
      <p class='lrg'>DIRECTORY</p>
    </div>

    
  </section>

</div>