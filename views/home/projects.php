<div class='content'>

  <img class='home' src="<?= FRONT_ASSETS ?>img/home.png">
  <section class='main projects_page'>
    <div class='page_title'>
      <p class='lrg'>VIDEO WALL<span>SOLUTIONS</span></p>
    </div>

    <div class='projs'>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj1.jpg');"></div>
        <p>MONTICELLO MOTOR CLUB</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj2.jpg');"></div>
        <p>RITZ CARLTON</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj3.jpg');"></div>
        <p>TEANECK CINEMAS</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj4.jpg');"></div>
        <p>DIOR SAN FRANCISCO</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj5.jpg');"></div>
        <p>RADISSON BLU</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj6.jpg');"></div>
        <p>LEXUS MADISON SQ. GARDEN</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj7.jpg');"></div>
        <p>LVMH EXECUTIVE OFFICES</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj8.jpg');"></div>
        <p>BOEING</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj9.jpg');"></div>
        <p>NOVOTEL HOTEL</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj10.jpg');"></div>
        <p>TAG HEUER</p>
      </div>
      <div class='card proj'>
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/projects/proj11.jpg');"></div>
        <p>ARMANI / 5TH AVENUE</p>
      </div>
    </div>

    <div class='show_proj'>
      <h5>x</h5>
      <div></div>
      <p></p>
      <h5 class='btm'>x</h5>
    </div>
  </section>

  <script type="text/javascript">
    $(document).on('click', '.proj', function(){
      var src = $(this).children('div').css('background-image');
      var txt = $(this).children('p').html();

      $('.show_proj').fadeIn();
      $('.show_proj').children('div').css('background-image', src);
      $('.show_proj').children('p').html(txt);
    });

    $(document).on('click', 'h5', function(){
      $('.show_proj').fadeOut();
    });
  </script>

</div>