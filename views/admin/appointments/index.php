<div class="form-group">
    <label>Show only from this office:</label>
    <select class="filter-offices form-control">
        <? foreach($model->offices as $office){ ?>
            <option value="<?=$office->O?>" <?=$office->O == $model->office_id ? 'selected' : ''?> ><?=$office->OFN?></option>
        <? }?>
    </select>
</div>
    <?php
 if(count($model->appointments)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="20%">Office</th>
           <th width="30%">Patient Name</th>
           <th width="10%">Appt Time</th>
            <th width="10%">Appointment Length</th>
            <th width="10%">Doctor</th>
           <th width="10%">Status</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->appointments as $obj){ ?>
       <tr>
        <td>
            <a href="<?php echo ADMIN_URL; ?>appointments/update/<?=$obj->APID?>">
                <span class="office-name" data-office="<?=$obj->O?>"><?=$obj->O?></span>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>appointments/update/<?=$obj->APID?>">
                <? if(!$obj->ISNP) {?>
                <span class="patient-name" data-patient="<?=$obj->PID?>"><?=$obj->PID?></span>
                <?} else {?>
                    New Patient
                <? }?>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>appointments/update/<?=$obj->APID?>">
                <?=date('m/d/y h:m A',strtotime($obj->APD))?>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>appointments/update/<?=$obj->APID?>">
                <?=$obj->APL." Minutes"?>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>appointments/update/<?=$obj->APID?>">
                <span class="doctor-name" data-doctor="<?=$obj->PDR?>"><?=$obj->PDR?></span>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>appointments/update/<?=$obj->APID?>">
                <?=$obj->APS?>
            </a>
        </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'patients';?>';
    var total_pages = <?= $model->pagination['total_pages'];?>;
    var page = <?= $model->pagination['current_page_index'];?>;

    $(document).ready(function(){
        $('select.filter-offices').on('change',function(){
            window.location.href = window.location.origin + window.location.pathname + '?office='+this.value;
        });
    });
</script>