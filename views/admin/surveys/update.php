  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
      <li role="presentation"><a href="#questions-tab" aria-controls="questions" role="tab" data-toggle="tab">Question Details</a></li>

    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general">
        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
                        <input type="hidden" name="id" value="<?php echo $model->survey->id; ?>" />
                        <h4>General</h4>
                        <div class="form-group">
                            <label>Title</label>
                            <?php echo $model->form->editorFor("title"); ?>
                        </div>
                        <div class="form-group">
                            <label>Display Order</label>
                            <?php echo $model->form->editorFor("display_order"); ?>
                        </div>
                        <div class="form-group">
                            <label>Background Color</label>
                            <?php echo $model->form->textBoxFor('background_color',['class'=>'jscolor']);?>
                        </div>
                        <div class="form-group">
                            <label>Questions</label>
                            <select multiple data-id='questions' id='questions_select'>
                            <!-- <select multiple name='questions[]'> -->
                                <? foreach($model->questions as $question) { ?>
                                <? $selected = in_array($question->id,$model->survey->questions)?'selected':''?>
                                <? $selected = ''?>
                                <option value='<?=$question->id?>' <?=$selected?>><?=$question->text?></option>
                                <? } ?>
                            </select>
                            <input type='hidden' id='questions' name='questions' value='<?=json_encode($model->survey->questions)?>' />
                        </div>
                        <button type="submit" class="btn btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="questions-tab">
          <div class="col-md-24">
              <div class="box">
                <h4>Question Details</h4>
                <div class="form-group">
                    <label>Question:</label>
                    <select id='question_select' name="question['id']" >
                        <? foreach($model->questions as $q) { ?>
                            <option value='<?=$q->id?>'><?=$q->text?></option>
                        <? } ?>
                    </select>
                </div>
                <a href='<?=ADMIN_URL."questions/update/0?redirectTo={$model->survey->id}"?>'>New</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th width="75%">Answers</th>
                            <th>% Selected</th>
                            <th>No. Selected</th>
                        </tr>
                    </thead>
                    <tbody class='answers'>
                    </tbody>
                </table>  
              </div>
          </div>
      </div>
    
    </div>
  </div>

<?php echo footer(); ?>
<script src="<?=ADMIN_JS.'jscolor.min.js'?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_JS?>chosen.order.jquery.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.min.css" />
<script type='text/javascript'>
    $(document).ready(function() {

        $("select[multiple]").chosen({width:'100%'});
        $('form').submit(function(){
            $("select[multiple]").each(function(){
                var id= $(this).data('id');
                $('input#'+id).val('{'+$(this).getSelectionOrder()+'}');
            });
        });
        <? if(count($model->survey->questions)) {?>
            ChosenOrder.setSelectionOrder($('#questions_select'),<?=json_encode($model->survey->questions)?>,true);
        <? } ?>
        $(".edit_answer").click( function(e) {
            e.preventDefault();
            $("#answer" + $(this).data('answer')).toggle();
            $("#edit_answer" + $(this).data('answer')).toggle();
        });
        $("#question_select").change(function(){
            $.get('/admin/surveys/load_question',{id:$(this).val()},function(data){
                $('.answers').html('');
                for(let i = 0; i < data.answers.length; i++){
                    $('.answers').append('<tr><td>'+data.answers[i]['text']+'</td><td>'+data.answers[i]['percent']+'%</td><td>'+data.answers[i]['number']+'</td></tr>');
                }
            })
        }).change();
        // $(".remove_answer_btn").click( function(e) {
        //     e.preventDefault();
        //     id = $(this).data('id');
        //     $(this).parents('.answer').remove();
        //     $.post('/admin/questions/remove_answer', {id: id}, function (response) {
        //         console.log(response);
        //     });
        // });
        // $(document).on('click',".remove_question_btn", function(e) {
        //     e.preventDefault();
        //     id = $(this).data('id');
        //     $('#question_select option[value='+id+']').remove();
        //     $('#question_select').val(0).change();
        //     $.post('/admin/questions/remove_question', {id: id}, function (response) {
        //         console.log(response);
        //     });
        // });

    });
</script>