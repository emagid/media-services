<div class="row">
    <div class="col-md-24">
        <div class="box">
            <form action="/admin/videos/index" method="GET" enctype="multipart/form-data">
              <div class="form-group">
                <label>Filter videos by page</label>
                <select name="type">
                  <option value='all'>Show All </option>
                  <? foreach(\Model\Video::$types as $t => $type) { ?>
                    <option value="<?=$t?>" <?=(isset($_GET['type']) && $_GET['type'] == $t)?'selected':''?>><?=$type?></option>
                  <? } ?>
                </select>
              </div>
            </form>
        </div>
    </div>
</div>
<div class='row'>
  <?php if(count($model->videos)>0): ?>
    <div class="box box-table">
      <table class="table">
        <thead>
          <tr>
            <th width="30%">Title</th>
            <th width="10%">Page</th> 
            <th width="30%">Thumbnail</th>
            <th width="15%" class="text-center">Edit</th>
            <th width="15%" class="text-center">Delete</th> 
          </tr>
        </thead>
        <tbody>
          <? foreach($model->videos as $obj){ ?>
          <tr>
            <td>
              <a href="<?php echo ADMIN_URL; ?>videos/update/<?php echo $obj->id; ?>">
                <?php echo $obj->title;?>
              </a>
            </td>
            <td>
              <a href="<?php echo ADMIN_URL; ?>videos/update/<?php echo $obj->id; ?>">
                <?php echo \Model\Video::$types[$obj->page];?>
              </a>
            </td>
            <td>
              <a href="<?php echo ADMIN_URL; ?>videos/update/<?php echo $obj->id; ?>">
                <img src="/content/uploads/videos/<?php echo $obj->image;?>" height="60px"/> 
              </a>
            </td> 
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>videos/update/<?= $obj->id ?>">
                <i class="icon-pencil"></i> 
              </a>
            </td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>videos/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                <i class="icon-cancel-circled"></i> 
              </a>
            </td>
          </tr>
        <? } ?>
    </tbody>
  </table>
    <div class="box-footer clearfix">
      <div class='paginationContent'></div>
    </div>
  </div>
  <?php endif; ?>
</div>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'videos';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
  $(window).ready(function(){
    $("form :input").change(function(){
      $("form").submit();
    })
  });
</script>
