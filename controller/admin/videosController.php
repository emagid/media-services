<?php
 
class VideosController extends adminController {
	
	function __construct(){
		parent::__construct("Video");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;
		
		$params['queryOptions']['orderBy'] = "page ASC, display_order ASC";
		if(isset($_GET['type']) && $_GET['type'] != 'all' && $_GET['type'] != ''){
			$params['queryOptions']['where'] = "page = {$_GET['type']}";
		}


		parent::index($params);
	}

	function update(Array $arr = []){
		 
      
		parent::update($arr);
	}
  
}