<?php

use Twilio\Rest\Client;

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function projects(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function project(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function wheel(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function medical(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }

    
}