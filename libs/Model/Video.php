<?php

namespace Model;

class Video extends \Emagid\Core\Model {
    static $tablename = "video";

    public static $fields  =  [
    	'title'=>['required'=>true],
    	'image',
        'video',
        'background_color',
        'page',
        'display_order'
    ];

    public static $types = [
        1=>"Minutes",
        // 2=>"Employee",
        3=>"Employee Stories"
    ];
}
























