<?php

namespace Model;

class Banner extends \Emagid\Core\Model {
    static $tablename = "public.banner";

    public static $fields  =  [
    	'title'=>['required'=>true],
    	'image',
        'link',
        'featured_id',
        'banner_order'
    ];    

    public function getBannerMap(){
        switch($this->featured_id){
            case 0:
                echo 'Main Banner';break;
            case 1:
                echo 'Featured 1';break;
            case 2:
                echo 'Featured 2';break;
            case 3:
                echo 'Featured 3';break;
            case 4:
                echo 'Featured 4';break;
            case 5:
                echo 'Featured 5';break;
            case 6:
                echo 'Featured 6';break;
        }
    }
}
























