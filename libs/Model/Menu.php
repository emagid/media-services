<?php

namespace Model;

class Menu extends \Emagid\Core\Model {
    static $tablename = "menu";

    public static $fields  =  [
    	'title'=>['required'=>true],
    	'image',
    	'type',
    	'display_order',
    	'kiosk_id'
    ];
}
























